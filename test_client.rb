require 'socket'
load 'policy_def.rb'

module Policy
	class Client
		def self.prefetch(host, port=843)
			response = ""
		
			begin
				TCPSocket.open(host, port) do |s|
					s.puts(PolicyDef::REQUEST)
					response = s.gets.strip;
				end
			rescue => ex
				puts "Prefetch policy file failed:"
				puts ex.message;
			end
			raise "Failed to fetch policy file" if !response.eql?PolicyDef::RESPONSE 
			return true
		end
	end
end

p 'policy file valid' if Policy::Client::prefetch('127.0.0.1')
