require 'socket'
load 'policy_Def.rb'

module Policy
	class Server
		def self.run(host, port=843)
			p "Starting policy server (%s:%d)" % [host, port]
			server = TCPServer.open(host,port)
			loop {
				client = server.accept
				if (client)
					p 'Client connected: ' + client.addr.to_s
					begin
						req = client.gets.strip
						if req.eql?PolicyDef::REQUEST
							client.puts PolicyDef::RESPONSE
							p 'Client handled: ' + client.addr.to_s
						end
					rescue
						p 'Error handling client: ' + client.addr.to_s	
					end
				end	
			}
		end
	end
end


Policy::Server::run('127.0.0.1')
